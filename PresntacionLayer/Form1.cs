﻿using System;
using System.Windows.Forms;

namespace PresntacionLayer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dgwPeople.DataSource = ModeloNegocio.People.Get();

            txtPOST.Text = Servicios.Service.GetPost();
        }
    }
}
