﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModeloNegocio
{
    public class People
    {
        public static List<Datos.people> Get()
        {
            using (Datos.capasEntities db=new Datos.capasEntities())
            {
                return db.people.ToList();
            }
        }
    }
}
